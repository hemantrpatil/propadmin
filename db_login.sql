/*
SQLyog Community
MySQL - 5.6.25-log : Database - orpm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `orpm`;

/*Table structure for table `membership_users` */

DROP TABLE IF EXISTS `membership_users`;

CREATE TABLE `membership_users` (
  `memberID` varchar(20) NOT NULL,
  `passMD5` varchar(40) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `signupDate` date DEFAULT NULL,
  `groupID` int(10) unsigned DEFAULT NULL,
  `isBanned` tinyint(4) DEFAULT NULL,
  `isApproved` tinyint(4) DEFAULT NULL,
  `custom1` text,
  `custom2` text,
  `custom3` text,
  `custom4` text,
  `comments` text,
  `pass_reset_key` varchar(100) DEFAULT NULL,
  `pass_reset_expiry` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`memberID`),
  KEY `groupID` (`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `membership_users` */

insert  into `membership_users`(`memberID`,`passMD5`,`email`,`signupDate`,`groupID`,`isBanned`,`isApproved`,`custom1`,`custom2`,`custom3`,`custom4`,`comments`,`pass_reset_key`,`pass_reset_expiry`) values ('guest',NULL,NULL,'2016-02-18',1,0,1,NULL,NULL,NULL,NULL,'Anonymous member created automatically on 2016-02-18',NULL,NULL);
insert  into `membership_users`(`memberID`,`passMD5`,`email`,`signupDate`,`groupID`,`isBanned`,`isApproved`,`custom1`,`custom2`,`custom3`,`custom4`,`comments`,`pass_reset_key`,`pass_reset_expiry`) values ('root','63a9f0ea7bb98050796b649e85481845','ved71714@gmail.com','2016-02-18',2,0,1,NULL,NULL,NULL,NULL,'Admin member created automatically on 2016-02-18',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
