package webService;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class RESTConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> set = new HashSet<>();
        set.add(CORS1Filter.class);
        set.add(FeedService.class);

        // wrong
        //set.add(super.getClass());
        //return super.getClasses();

        // corrected
       set.addAll(super.getClasses());
       System.out.println("REst  config called");
       return set;
    }    
}