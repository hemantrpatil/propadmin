package webService;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import sun.misc.BASE64Decoder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import dto.ChartData;
import dto.FirmDeatilsByEvaluator;
import dto.SurveyData;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;
import model.Branch;
import model.Lead;
import model.LeadFollowup;
import model.ProjectManager;
import model.Staff;
import model.User;
import model.property;

@Path("/WebService")
public class FeedService {
	

	@GET
	@Path("/GetLoginDetails/{varEmail}/{varPassword}")
	@Produces("application/json")
	public String GetLoginDetails(@PathParam("varEmail") String varEmail,
		    @PathParam("varPassword") String varPassword) 
	{
		String feeds  = null;
		String decodedAuth = "";
		try 
		{
			byte[] bytes = null;
	        try {
	        	System.out.println(varPassword);
	            bytes = new BASE64Decoder().decodeBuffer(varPassword);
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        decodedAuth = new String(bytes);
	        System.out.println(decodedAuth);
	        User user = null;
			ProjectManager projectManager= new ProjectManager();
			user = projectManager.GetLoginDetails(varEmail,varPassword);
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(user));
			feeds = gson.toJson(user);
			

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	
	@GET
	
	@Path("/GetOraganizations/")
	@Produces("application/json")
	public Response GetOraganizations()
	{
		String feeds  = null;
		try 
		{
			//User user = null;
			ProjectManager projectManager= new ProjectManager();
			ArrayList <String> organizations = projectManager.GetOraganizations();
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(organizations));
			feeds = gson.toJson(organizations);
			Response res;
				
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
		return Response.status(200)
				.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE").entity(feeds).build();
	}
	
@GET
	
	@Path("/GetChartData/")
	@Produces("application/json")
	public Response GetChartData()
	{
		String feeds  = null;
		try 
		{
			//User user = null;
			ProjectManager projectManager= new ProjectManager();
			ArrayList <ChartData> organizationsData = projectManager.GetChartData();
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(organizationsData));
			feeds = gson.toJson(organizationsData);
			Response res;
				
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
		return Response.status(200)
				.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE").entity(feeds).build();
	}
	
	
	@GET
	@Path("/GetFirmDetails/{varEvaluatorID}")
	@Produces("application/json")
	
	public String GetFirmDetails(@PathParam("varEvaluatorID") String varEvaluatorID
		   )
	{
		String feeds  = null;
		
		try 
		{
			ArrayList<FirmDeatilsByEvaluator> allfirms = null;
			ProjectManager projectManager= new ProjectManager();
			allfirms = projectManager.GetFirmDetails(varEvaluatorID);
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(allfirms));
			feeds = gson.toJson(allfirms);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	
	
	@POST
	@Path("/WriteSurveyData")
	@Consumes("application/json")
	public void WriteSurveyData(String message)
	{
		String feeds  = null;
		System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
		try 
		{
			ArrayList<User> feedData = null;
			
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			SurveyData sdata = gson.fromJson(message, SurveyData.class);
			ProjectManager projectManager= new ProjectManager();
			System.out.println(sdata);
			 projectManager.WriteSurveyData(sdata);
			System.out.println(sdata);
			//feeds = gson.toJson(feedData);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
	}
	
	@POST
	@Path("/InsertLead")
	@Consumes("application/json")
	public void InsertLead(String message)
	{
		String feeds  = null;
		System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
		try 
		{
			ArrayList<User> feedData = null;
			
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			Lead sdata = gson.fromJson(message, Lead.class);
			ProjectManager projectManager= new ProjectManager();
			System.out.println(sdata);
			 projectManager.InsertLead(sdata);
			System.out.println(sdata);
			//feeds = gson.toJson(feedData);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
	}
	

	@GET
	@Path("/GetAllLeads")
	@Produces("application/json")
	
	public String GetAllLeads(@Context HttpHeaders header,@Context HttpServletResponse response) 
		 
	{
		MultivaluedMap<String, String> headerParams = header.getRequestHeaders();
	    String userKey = "Authorization";
	    headerParams.get(userKey);
	    System.out.println( headerParams.get(userKey));
		String feeds  = null;
		
		try 
		{
			ArrayList<Lead> allfirms = null;
			ProjectManager projectManager= new ProjectManager();
			allfirms = projectManager.GetAllLeads();
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(allfirms));
			feeds = gson.toJson(allfirms);
			response.setHeader("Authorization", headerParams.get(userKey).toString());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	
	@POST
	@Path("/InsertBranch")
	@Consumes("application/json")
	public void InsertBranch(String message)
	{
		String feeds  = null;
		System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
		try 
		{
			
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			Branch sdata = gson.fromJson(message, Branch.class);
			ProjectManager projectManager= new ProjectManager();
			System.out.println(sdata);
			 projectManager.InsertBranch(sdata);
			System.out.println(sdata);
			//feeds = gson.toJson(feedData);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
	}
	
	@POST
	@Path("/InsertProperty")
	@Consumes("application/json")
	public void InsertProperty(String message)
	{
		String feeds  = null;
		System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
		try 
		{
			
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			property sdata = gson.fromJson(message, property.class);
			ProjectManager projectManager= new ProjectManager();
			System.out.println(sdata);
			 projectManager.InsertProperty(sdata);
			System.out.println(sdata);
			//feeds = gson.toJson(feedData);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
	}
	
	@GET
	@Path("/GetAllProprety")
	@Produces("application/json")
	
	public String GetAllProprety() 
		 
	{
		String feeds  = null;
		
		try 
		{
			ArrayList<property> allfirms = null;
			ProjectManager projectManager= new ProjectManager();
			allfirms = projectManager.GetAllProprety();
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(allfirms));
			feeds = gson.toJson(allfirms);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	
	
	@GET
	@Path("/GetAllBranches")
	@Produces("application/json")
	
	public String GetAllBranches() 
		 
	{
		String feeds  = null;
		
		try 
		{
			ArrayList<Branch> allfirms = null;
			ProjectManager projectManager= new ProjectManager();
			allfirms = projectManager.GetAllBranches();
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(allfirms));
			feeds = gson.toJson(allfirms);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	
	@POST
	@Path("/InsertStaffMember")
	@Consumes("application/json")
	public void InsertStaffMember(String message)
	{
		String feeds  = null;
		System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
		try 
		{
			
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			Staff sdata = gson.fromJson(message, Staff.class);
			ProjectManager projectManager= new ProjectManager();
			System.out.println(sdata);
			 projectManager.InsertStaffMember(sdata);
			System.out.println(sdata);
			//feeds = gson.toJson(feedData);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
	}
	
	@GET
	@Path("/GetAllStaff")
	@Produces("application/json")
	
	public String GetAllStaff() 
		 
	{
		String feeds  = null;
		
		try 
		{
			ArrayList<Staff> allfirms = null;
			ProjectManager projectManager= new ProjectManager();
			allfirms = projectManager.GetAllStaff();
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(allfirms));
			feeds = gson.toJson(allfirms);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	
	@POST
	@Path("/InsertFollowup")
	@Consumes("application/json")
	public void InsertFollowup(String message)
	{
		String feeds  = null;
		System.out.println("Insert String2>>>>>>>>>>>>>>>"+message);
		try 
		{
			
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			LeadFollowup sdata = gson.fromJson(message, LeadFollowup.class);
			ProjectManager projectManager= new ProjectManager();
			System.out.println(sdata);
			 projectManager.InsertFollowup(sdata);
			System.out.println(sdata);
			//feeds = gson.toJson(feedData);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		//return feeds;
	}

	@GET
	@Path("/GetFollowupDetails/{varLeadId}")
	@Produces("application/json")
	public String GetFollowupDetails(@PathParam("varLeadId") String varLeadId) 
	 
	{
		String feeds  = null;
		
		try 
		{
			ArrayList<LeadFollowup> allfirms = null;
			ProjectManager projectManager= new ProjectManager();
			allfirms = projectManager.GetFollowupDetails(varLeadId);
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(allfirms));
			feeds = gson.toJson(allfirms);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	@GET
	@Path("/GetMenuDetails")
	@Produces("text/html")
	public String GetMenuDetails() 
	 
	{
		String feeds  = null;
		
		try 
		{
			String allfirms = null;
			ProjectManager projectManager= new ProjectManager();
			allfirms = projectManager.GetMenuDetails();
			//StringBuffer sb = new StringBuffer();
			Gson gson = new Gson();
			System.out.println(gson.toJson(allfirms));
			feeds = allfirms;

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return feeds;
	}
	
}
