package model;

public class property {
	private String property_type;
	private String property_name;
	private String apartment_facility;
	private String residential_property;
	private String commercial_property;
	private String  flat_type;
	private String  pg_type;
	private String  pg_category_type;
	private String  property_Add;
	private Integer property_id;
	private String state;
	private String city;
	private String pincode;
	private String building_name;
	private String flat_shop_unit_no;
	private String wing;
	private String lene1;
	private String lene2;
	private String society_amenities;
	private String property_photo;
	private String keyrdate;
	private String followup_service;
	public String getProperty_type() {
		return property_type;
	}
	public void setProperty_type(String property_type) {
		this.property_type = property_type;
	}
	public String getProperty_name() {
		return property_name;
	}
	public void setProperty_name(String property_name) {
		this.property_name = property_name;
	}
	public String getApartment_facility() {
		return apartment_facility;
	}
	public void setApartment_facility(String apartment_facility) {
		this.apartment_facility = apartment_facility;
	}
	public String getResidential_property() {
		return residential_property;
	}
	public void setResidential_property(String residential_property) {
		this.residential_property = residential_property;
	}
	public String getCommercial_property() {
		return commercial_property;
	}
	public void setCommercial_property(String commercial_property) {
		this.commercial_property = commercial_property;
	}
	public String getFlat_type() {
		return flat_type;
	}
	public void setFlat_type(String flat_type) {
		this.flat_type = flat_type;
	}
	public String getPg_type() {
		return pg_type;
	}
	public void setPg_type(String pg_type) {
		this.pg_type = pg_type;
	}
	public String getPg_category_type() {
		return pg_category_type;
	}
	public void setPg_category_type(String pg_category_type) {
		this.pg_category_type = pg_category_type;
	}
	public String getProperty_Add() {
		return property_Add;
	}
	public void setProperty_Add(String property_Add) {
		this.property_Add = property_Add;
	}
	public Integer getProperty_id() {
		return property_id;
	}
	public void setProperty_id(Integer property_id) {
		this.property_id = property_id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getBuilding_name() {
		return building_name;
	}
	public void setBuilding_name(String building_name) {
		this.building_name = building_name;
	}
	public String getFlat_shop_unit_no() {
		return flat_shop_unit_no;
	}
	public void setFlat_shop_unit_no(String flat_shop_unit_no) {
		this.flat_shop_unit_no = flat_shop_unit_no;
	}
	public String getWing() {
		return wing;
	}
	public void setWing(String wing) {
		this.wing = wing;
	}
	public String getLene1() {
		return lene1;
	}
	public void setLene1(String lene1) {
		this.lene1 = lene1;
	}
	public String getLene2() {
		return lene2;
	}
	public void setLene2(String lene2) {
		this.lene2 = lene2;
	}
	public String getSociety_amenities() {
		return society_amenities;
	}
	public void setSociety_amenities(String society_amenities) {
		this.society_amenities = society_amenities;
	}
	public String getProperty_photo() {
		return property_photo;
	}
	public void setProperty_photo(String property_photo) {
		this.property_photo = property_photo;
	}
	public String getKeyrdate() {
		return keyrdate;
	}
	public void setKeyrdate(String keyrdate) {
		this.keyrdate = keyrdate;
	}
	public String getFollowup_service() {
		return followup_service;
	}
	public void setFollowup_service(String followup_service) {
		this.followup_service = followup_service;
	}
	

}
