package model;

public class LeadFollowup {
	private int id;
	private int lead_id;
	private String date;
	private String next_followup_date;
	private String details;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLead_id() {
		return lead_id;
	}
	public void setLead_id(int lead_id) {
		this.lead_id = lead_id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNext_followup_date() {
		return next_followup_date;
	}
	public void setNext_followup_date(String next_followup_date) {
		this.next_followup_date = next_followup_date;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	
	
	

}
