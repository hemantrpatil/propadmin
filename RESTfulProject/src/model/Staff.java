package model;

public class Staff {
	private String name;
	private String email_id;
	private String contact_no;
	private String address;
	private String  city;
	private String  state;
	private String  zip;
	private String  location;
	private Integer id;
	private String  permanent_address;
	private String  landline_no;
	private String  comment;
	private String  address_proof_list;
	private String  id_proof_list;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getAdress() {
		return address;
	}
	public void setAdress(String adress) {
		this.address = adress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPermanent_address() {
		return permanent_address;
	}
	public void setPermanent_address(String permanent_address) {
		this.permanent_address = permanent_address;
	}
	public String getLandline_no() {
		return landline_no;
	}
	public void setLandline_no(String landline_no) {
		this.landline_no = landline_no;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getAddress_proof_list() {
		return address_proof_list;
	}
	public void setAddress_proof_list(String address_proof_list) {
		this.address_proof_list = address_proof_list;
	}
	public String getId_proof_list() {
		return id_proof_list;
	}
	public void setId_proof_list(String id_proof_list) {
		this.id_proof_list = id_proof_list;
	}

}
