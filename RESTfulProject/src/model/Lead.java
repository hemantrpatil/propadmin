package model;

public class Lead {
	private String visitor_name;
	private String email_id;
	private String contact_no;
	private String lead_type;
	private Integer max_rent;
	private Integer max_deposit;
	private String  references;
	private String  req_start_date;
	private String  property_type;
	private String  property_type_details;
	private String  location;
	private String remarks;
	private Integer id;
	private Integer min_rent;
	private Integer min_deposit;
	private String landline_no;
	private String  lead_date;
	
	public Integer getMin_rent() {
		return min_rent;
	}
	public void setMin_rent(Integer min_rent) {
		this.min_rent = min_rent;
	}
	public Integer getMin_deposit() {
		return min_deposit;
	}
	public void setMin_deposit(Integer min_deposit) {
		this.min_deposit = min_deposit;
	}
	public String getLandline_no() {
		return landline_no;
	}
	public void setLandline_no(String landline_no) {
		this.landline_no = landline_no;
	}

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVisitor_name() {
		return visitor_name;
	}
	public void setVisitor_name(String visitor_name) {
		this.visitor_name = visitor_name;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getLead_type() {
		return lead_type;
	}
	public void setLead_type(String lead_type) {
		this.lead_type = lead_type;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getMax_rent() {
		return max_rent;
	}
	public void setMax_rent(Integer max_rent) {
		this.max_rent = max_rent;
	}
	public Integer getMax_deposit() {
		return max_deposit;
	}
	public void setMax_deposit(Integer max_deposit) {
		this.max_deposit = max_deposit;
	}
	public String getReq_start_date() {
		return req_start_date;
	}
	public void setReq_start_date(String req_start_date) {
		this.req_start_date = req_start_date;
	}
	public String getProperty_type() {
		return property_type;
	}
	public void setProperty_type(String property_type) {
		this.property_type = property_type;
	}
	public String getProperty_type_details() {
		return property_type_details;
	}
	public void setProperty_type_details(String property_type_details) {
		this.property_type_details = property_type_details;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	
	
	public String getReferences() {
		return references;
	}
	public void setReferences(String references) {
		this.references = references;
	}
	public String getLead_date() {
		return lead_date;
	}
	public void setLead_date(String lead_date) {
		this.lead_date = lead_date;
	}
	
	
}
