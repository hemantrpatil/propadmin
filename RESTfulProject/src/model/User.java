package model;

public class User {

private String companyName;
private String email;
private String contactName;
private String mobile;
private String deskPhone;
private String addressLine1;
private String addressLine2;
private String addressLine3;
private String city;
private String zip;
private String state;
private String name;
private String userID;
private String organization;
private String password;

public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getContactName() {
	return contactName;
}
public void setContactName(String contactName) {
	this.contactName = contactName;
}
public String getDeskPhone() {
	return deskPhone;
}
public void setDeskPhone(String deskPhone) {
	this.deskPhone = deskPhone;
}
public String getAddressLine3() {
	return addressLine3;
}
public void setAddressLine3(String addressLine3) {
	this.addressLine3 = addressLine3;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}

private String country;

public String getCompanyName() {
	return companyName;
}
public void setCompanyName(String companyName) {
	this.companyName = companyName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getMobile() {
	return mobile;
}
public void setMobile(String mobile) {
	this.mobile = mobile;
}
public String getAddressLine1() {
	return addressLine1;
}
public void setAddressLine1(String addressLine1) {
	this.addressLine1 = addressLine1;
}
public String getAddressLine2() {
	return addressLine2;
}
public void setAddressLine2(String addressLine2) {
	this.addressLine2 = addressLine2;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getZip() {
	return zip;
}
public void setZip(String zip) {
	this.zip = zip;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}

@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("companyName : " + companyName);
		return sb.toString();
	}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getUserID() {
	return userID;
}
public void setUserID(String userID) {
	this.userID = userID;
}
public String getOrganization() {
	return organization;
}
public void setOrganization(String organization) {
	this.organization = organization;
}

}
