package model;

import java.sql.Connection;
import java.util.ArrayList;

import dao.Database;

import dao.Project;
import dto.ChartData;
import dto.FirmDeatilsByEvaluator;
import dto.SurveyData;
//import dto.FeedObjects;

public class ProjectManager {
	
	
	
	public ArrayList<FirmDeatilsByEvaluator> GetFirmDetails(String evaluatorID)throws Exception {
		ArrayList<FirmDeatilsByEvaluator> allfirms= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				allfirms=project.GetFirmDetails(connection, evaluatorID);
		
		} catch (Exception e) {
			throw e;
		}
		return allfirms;
	}
	public User GetLoginDetails(String email, String password)throws Exception {
		User user= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				user=project.GetLoginDetails(connection, email,password);
		
		} catch (Exception e) {
			throw e;
		}
		return user;
	}
	public ArrayList<String> GetOraganizations()throws Exception {
		ArrayList<String> organizations= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				organizations=project.GetOraganizations(connection);
		
		} catch (Exception e) {
			throw e;
		}
		return organizations;
	}
	
	public ArrayList<ChartData> GetChartData()throws Exception {
		ArrayList<ChartData> organizationsData= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				organizationsData=project.GetChartData(connection);
		
		} catch (Exception e) {
			throw e;
		}
		return organizationsData;
	}
	
	public void WriteSurveyData(SurveyData sdata){
		//ArrayList<User> feeds = null;
		try {
			System.out.println("b4 projmanager");    
			Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				project.addSurveyDetails(connection, sdata);
		System.out.println("after projmanager");
		} catch (Exception e) {
			
		
		}
	}
	
	
	public void InsertLead(Lead sdata){
		//ArrayList<User> feeds = null;
		try {
			System.out.println("b4 projmanager");    
			Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				project.InsertLead(connection, sdata);
		System.out.println("after projmanager");
		} catch (Exception e) {
			
		
		}
	}
	
	public ArrayList<Lead> GetAllLeads()throws Exception {
		ArrayList<Lead> allfirms= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				allfirms=project.GetAllLeads(connection);
		
		} catch (Exception e) {
			throw e;
		}
		return allfirms;
	}
	
	public void InsertBranch(Branch sdata){
		//ArrayList<User> feeds = null;
		try {
			System.out.println("b4 projmanager");    
			Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				project.InsertBranch(connection, sdata);
		System.out.println("after projmanager");
		} catch (Exception e) {
			
		
		}
	}
	
	public void InsertProperty(property sdata){
		//ArrayList<User> feeds = null;
		try {
			System.out.println("b4 Insert Property");    
			Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				project.InsertProperty(connection, sdata);
		System.out.println("after Insert Property");
		} catch (Exception e) {
			
		
		}
	}
	
	public ArrayList<property> GetAllProprety()throws Exception {
		ArrayList<property> allfirms= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				allfirms=project.GetAllProprety(connection);
		
		} catch (Exception e) {
			throw e;
		}
		return allfirms;
	}
	
	public ArrayList<Branch> GetAllBranches()throws Exception {
		ArrayList<Branch> allfirms= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				allfirms=project.GetAllBranches(connection);
		
		} catch (Exception e) {
			throw e;
		}
		return allfirms;
	}
	
	public void InsertStaffMember(Staff sdata){
		//ArrayList<User> feeds = null;
		try {
			System.out.println("b4 projmanager");    
			Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				project.InsertStaffMember(connection, sdata);
		System.out.println("after projmanager");
		} catch (Exception e) {
			
		
		}
	}
	
	public ArrayList<Staff> GetAllStaff()throws Exception {
		ArrayList<Staff> allfirms= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				allfirms=project.GetAllStaff(connection);
		
		} catch (Exception e) {
			throw e;
		}
		return allfirms;
	}
	
	public void InsertFollowup(LeadFollowup sdata){
		//ArrayList<User> feeds = null;
		try {
			System.out.println("b4 projmanager");    
			Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				project.InsertFollowup(connection, sdata);
		System.out.println("after projmanager");
		} catch (Exception e) {
			
		
		}
	}
	
	public ArrayList<LeadFollowup> GetFollowupDetails(String  varLeadId)throws Exception {
		ArrayList<LeadFollowup> allfirms= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				allfirms=project.GetFollowupDetails(connection,varLeadId);
		
		} catch (Exception e) {
			throw e;
		}
		return allfirms;
	}
	public String GetMenuDetails()throws Exception {
		String allfirms= null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
				Project project= new Project();
				allfirms=project.GetMenuDetails(connection);
		
		} catch (Exception e) {
			throw e;
		}
		return allfirms;
	}
	
	
}
