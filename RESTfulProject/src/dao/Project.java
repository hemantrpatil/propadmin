package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import dto.ChartData;
import dto.FirmDeatilsByEvaluator;
import dto.SurveyData;
import model.Branch;
import model.Lead;
import model.LeadFollowup;
import model.Staff;
import model.User;
import model.property;


public class Project {
	
	
	
   public ArrayList<FirmDeatilsByEvaluator>  GetFirmDetails(Connection connection,String evaluatorID) throws Exception
	{
		ArrayList<FirmDeatilsByEvaluator>  allfirms =new ArrayList<FirmDeatilsByEvaluator>();
		try
		{
			//String uname = request.getParameter("uname");
			String query ="SELECT firm.FirmName, firm.FirmState, FirmPracticeType,firm.FirmCountry, eval.UserName as FirmEvaluator, firm.FirmID,eval.ID as EvaluatorID FROM testcustomersurvey.tblfirmdetails as firm" 
			+" inner join testcustomersurvey.tblmaplfirmnevaluator as map on firm.FirmID = map.FirmID"
			+" inner join testcustomersurvey.tbllogindetails as eval on eval.ID= map.EvaluatorID"
			+" where eval.id = " +  evaluatorID + ";";
			
			System.out.println(query);
			PreparedStatement ps = connection.prepareStatement(query);
			//ps.setString(1,uname);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				FirmDeatilsByEvaluator firm = new FirmDeatilsByEvaluator();
				firm.setFirmName(rs.getString("FirmName"));
				firm.setFirmState(rs.getString("FirmState"));
				firm.setFirmCountry(rs.getString("FirmCountry"));
				firm.setFirmPracticeType(rs.getString("FirmPracticeType"));
				firm.setFirmEvaluator(rs.getString("FirmEvaluator"));
				firm.setFirmFirmID(rs.getString("FirmID"));
				firm.setFirmEvaluatorID(rs.getString("EvaluatorID"));
				//user.setMobile(rs.getString("mobile_no"));
				allfirms.add(firm);
				
			}
			return allfirms;
		}
		catch(Exception e)
		{
			throw e;
		}
}

	
		public User GetLoginDetails(Connection connection,String email, String password) throws Exception
		{
			User user = new User();
			try
			{
				//String uname = request.getParameter("uname");
				String query ="SELECT email,memberID,passMD5 FROM tbl_membership_users where email = '" + email + "' and passMD5 = '" +  password +"';";
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				System.out.println(query);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					//User user = new User();
					user.setPassword(rs.getString("passMD5"));
					user.setEmail(rs.getString("email"));
					user.setUserID(rs.getString("memberID"));
					//user.setOrganization(rs.getString("Organization"));
					//user.setMobile(rs.getString("mobile_no"));
					
				}
				return user;
			}
			catch(Exception e)
			{
				throw e;
			}
	}
	
		public ArrayList<String> GetOraganizations(Connection connection) throws Exception
		{
			ArrayList<String> organizations = new ArrayList<String>();
			try
			{
				//String uname = request.getParameter("uname");
				String query ="SELECT Organization FROM tbllogindetails" ;
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				System.out.println(query);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					String organization= rs.getString("Organization");
					
					organizations.add(organization);
				}
				
				return organizations;
			}
			catch(Exception e)
			{
				throw e;
			}
	}
		public ArrayList<ChartData> GetChartData(Connection connection) throws Exception
		{
			ArrayList<ChartData> organizationsData = new ArrayList<ChartData>();
			try
			{
				//String uname = request.getParameter("uname");
				String query ="SELECT firmName,firmAverage,overallAverage FROM testcustomersurvey.tblfirmaverage;" ;
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				System.out.println(query);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					ChartData firm= new ChartData();
					firm.setFirmName(rs.getString("firmName"));
					firm.setFirmAverage(rs.getFloat("firmAverage"));	
					firm.setTotalAverage(rs.getFloat("overallAverage"));
					
					organizationsData.add(firm);
				}
				return organizationsData;
			}
			catch(Exception e)
			{
				throw e;
			}
	}
	public void addSurveyDetails(Connection connection,SurveyData sdata) throws SQLException{
		System.out.print("inside add survey details");
		String sqlCommand = "Insert into tblevlresults (UnderstandsOurObjectiveExpectations,Expertise,Responsiveness,Efficiency,tblevResultscol,FirmID,EvaluatorID,year) values( " 
				+sdata.getObjectives()+',' + sdata.getExpertise()+',' +sdata.getResponsiveness()+','+sdata.getEfficiancy()+','+ sdata.getXYZ() +',' + sdata.getFirmID() +',' + sdata.getuserID()+",'" + sdata.getYear()	+"');";
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//ResultSet re =ps.executeQuery();
	//System.out.print("yyuuyyu"+re.toString());
		
	}
	public void InsertLead(Connection connection,Lead sdata) throws SQLException{
		System.out.print("inside add survey details");
		//String sqlCommand = "INSERT INTO rentmanagement.tbl_lead(person_name,contact_number,remarks,leadtype_id,reference,email) VALUES ( '" +sdata.getVisitor_name()+ "','" + sdata.getContact_no()+  "','" +sdata.getRemarks()+ "'," +sdata.getLead_type()+ ",'"+ sdata.getReferences() +"','"+ sdata.getEmail_id() +"');";
	
		String sqlCommand = "INSERT INTO `rentmanagement`.`tbl_lead` (`person_name`,`reference`,`contact_number`,`remarks`,`lead_type`,"+
		"`email`,`req_start_date`,`location`,`property_type`,`property_type_detail`,`max_rent`,`max_deposit`,`min_rent`,`min_deposit`,`landline_no`)" +
			"VALUES('"+ sdata.getVisitor_name() +"','"+
						sdata.getReferences() +"','" +
						sdata.getContact_no() +"','" +
						sdata.getRemarks() +"','" +
						sdata.getLead_type() +"','" +
						sdata.getEmail_id() +"','" +
						sdata.getReq_start_date()+"','" +
						sdata.getLocation()+"','" +
						sdata.getProperty_type()+"','" +
						sdata.getProperty_type_details()+"'," +
						sdata.getMax_rent()+"," +
						sdata.getMax_deposit() + "," +
						sdata.getMin_rent()+"," +
						sdata.getMin_deposit()+",'" +
						sdata.getLandline_no()
						+ "')";
						
		System.out.print(sqlCommand);
		Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
		try {
			boolean insert = ps.execute(sqlCommand);
			System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//ResultSet re =ps.executeQuery();
	//System.out.print("yyuuyyu"+re.toString());
		
	}

	 public ArrayList<Lead>  GetAllLeads(Connection connection) throws Exception
		{
			ArrayList<Lead>  allfirms =new ArrayList<Lead>();
			try
			{
				//String uname = request.getParameter("uname");
				String query ="SELECT * FROM rentmanagement.tbl_lead";
				
				System.out.println(query);
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					Lead firm = new Lead();
					firm.setVisitor_name(rs.getString("person_name"));
					firm.setContact_no(rs.getString("contact_number"));
					firm.setProperty_type(rs.getString("property_type"));
					firm.setLocation(rs.getString("location"));
					firm.setLead_type(rs.getString("lead_type"));
					firm.setMax_rent(rs.getInt("max_rent"));
					firm.setMax_deposit(rs.getInt("max_deposit"));
					firm.setMin_rent(rs.getInt("min_rent"));
					firm.setMin_deposit(rs.getInt("min_deposit"));
					firm.setLandline_no(rs.getString("landline_no"));
					firm.setEmail_id(rs.getString("email"));
					firm.setProperty_type_details(rs.getString("property_type_detail"));
					firm.setReferences(rs.getString("reference"));
					firm.setRemarks(rs.getString("remarks"));
					firm.setReq_start_date(rs.getString("req_start_date"));
					firm.setLead_date(rs.getString("date"));
					
					firm.setId(rs.getInt("ID"));
								
					allfirms.add(firm);
					
				}
				return allfirms;
			}
			catch(Exception e)
			{
				throw e;
			}
	}
	
	 public void InsertBranch(Connection connection,Branch sdata) throws SQLException{
			System.out.print("inside add branch \n");
			//String sqlCommand = "INSERT INTO rentmanagement.tbl_lead(person_name,contact_number,remarks,leadtype_id,reference,email) VALUES ( '" +sdata.getVisitor_name()+ "','" + sdata.getContact_no()+  "','" +sdata.getRemarks()+ "'," +sdata.getLead_type()+ ",'"+ sdata.getReferences() +"','"+ sdata.getEmail_id() +"');";
		
			
							
			String sqlCommand = "INSERT INTO `rentmanagement`.`tbl_branch`"+ 
			"(`name`,`contact`,`email`,`address`,`city`,`state`,`zipcode`,`bank_name`,`bank_account_no`,`bank_ifsc_code`,`description`)"+
			"VALUES ('" +
						sdata.getBranch_name() +"','"+
						sdata.getContact_no()+"','"+
						sdata.getEmail_id()+"','"+
						sdata.getAdress()+"','"+
						sdata.getCity() +"','"+
						sdata.getState() +"','"+
						sdata.getZip() +"','"+
						sdata.getBank_name() +"','"+  
						sdata.getBank_account_no() +"','"+  
						sdata.getBank_ifsc_code() +"','"+  
						sdata.getDescription()
						+"')";
			
			
			
			System.out.print(sqlCommand);
			Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
			try {
				boolean insert = ps.execute(sqlCommand);
				System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//ResultSet re =ps.executeQuery();
		//System.out.print("yyuuyyu"+re.toString());
			
		}
	 
	 
	 public void InsertProperty(Connection connection,property sdata) throws SQLException{
			System.out.print("inside add property \n");
			
		
			
							
			String sqlCommand = "INSERT INTO `rentmanagement`.`tbl_property`"+ 
			"(`property_type`,`property_name`,`apartment_facility`,`residential_property`,`commercial_property`,`flat_type`,`pg_type`,`pg_category_type`,`property_Add`,`state`,`city`,`pincode`,`building_name`,`flat_shop_unit_no`,`wing`,`lene1`,`lene2`,`society_amenities`,`property_photo`,`keyrdate`,`followup_service`)"+
			"VALUES ('" +
						sdata.getProperty_type() +"','"+
						sdata.getProperty_name()+"','"+
						sdata.getApartment_facility()+"','"+
						sdata.getResidential_property() +"','"+
						sdata.getCommercial_property() +"','"+
						sdata.getFlat_type() +"','"+
						sdata.getPg_type() +"','"+
						sdata.getPg_category_type() +"','"+  
						sdata.getProperty_Add() +"','"+  
						sdata.getState() +"','"+  
						sdata.getCity() +"','"+ 
						sdata.getPincode() +"','"+ 
						sdata.getBuilding_name() +"','"+ 
						sdata.getFlat_shop_unit_no() +"','"+ 
						sdata.getWing() +"','"+ 
						sdata.getLene1() +"','"+ 
						sdata.getLene2() +"','"+ 
						sdata.getSociety_amenities() +"','"+ 
						sdata.getProperty_photo() +"','"+ 
						sdata.getKeyrdate() +"','"+ 
						sdata.getFollowup_service()
						+"')";
			
			
			
			System.out.print(sqlCommand);
			Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
			try {
				boolean insert = ps.execute(sqlCommand);
				System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//ResultSet re =ps.executeQuery();
		//System.out.print("yyuuyyu"+re.toString());
			
		}
	 
	 
	 public ArrayList<property>  GetAllProprety(Connection connection) throws Exception
		{
			ArrayList<property>  allfirms =new ArrayList<property>();
			try
			{
			
				String query ="SELECT * FROM rentmanagement.tbl_property";
				
				System.out.println(query);
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					property firm = new property();
					firm.setProperty_type(rs.getString("property_type"));
					firm.setProperty_name(rs.getString("property_name"));
					firm.setApartment_facility(rs.getString("apartment_facility"));
					firm.setResidential_property(rs.getString("residential_property"));					firm.setCity(rs.getString("city"));
					firm.setCommercial_property(rs.getString("commercial_property"));
					firm.setFlat_type(rs.getString("flat_type"));
					firm.setPg_type(rs.getString("pg_type"));
					firm.setPg_category_type(rs.getString("pg_category_type"));
					firm.setProperty_Add(rs.getString("property_add"));
					firm.setState(rs.getString("state"));			
					firm.setCity(rs.getString("city"));
					firm.setPincode(rs.getString("pincode"));
					firm.setBuilding_name(rs.getString("building_name"));
					firm.setFlat_shop_unit_no(rs.getString("flat_shop_unit_no"));
					firm.setWing(rs.getString("wing"));
					firm.setLene1(rs.getString("Lene1"));
					firm.setLene2(rs.getString("Lene2"));
					firm.setSociety_amenities(rs.getString("society_amenities"));
					firm.setProperty_photo(rs.getString("property_photo"));
					firm.setKeyrdate(rs.getString("keyrdate"));
					firm.setFollowup_service(rs.getString("followup_service"));
					allfirms.add(firm);
					
				}
				System.out.println(allfirms);
				return allfirms;
			}
			catch(Exception e)
			{
				throw e;
			}
	}
	 
	 
	 
	 
	 public ArrayList<Branch>  GetAllBranches(Connection connection) throws Exception
		{
			ArrayList<Branch>  allfirms =new ArrayList<Branch>();
			try
			{
				//String uname = request.getParameter("uname");
				String query ="SELECT * FROM rentmanagement.tbl_branch";
				
				System.out.println(query);
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					Branch firm = new Branch();
					firm.setBranch_name(rs.getString("name"));
					firm.setContact_no(rs.getString("contact"));
					firm.setEmail_id(rs.getString("email"));
					firm.setAdress(rs.getString("address"));
					firm.setCity(rs.getString("city"));
					firm.setState(rs.getString("state"));
					firm.setZip(rs.getString("zipcode"));
					firm.setId(rs.getInt("ID"));
					firm.setBank_name(rs.getString("bank_name"));
					firm.setBank_account_no(rs.getString("bank_account_no"));
					firm.setBank_ifsc_code(rs.getString("bank_ifsc_code"));			
					firm.setDescriptiion(rs.getString("description"));
					allfirms.add(firm);
					
				}
				System.out.println(allfirms);
				return allfirms;
			}
			catch(Exception e)
			{
				throw e;
			}
	}
	
	 public void InsertStaffMember(Connection connection,Staff sdata) throws SQLException{
			System.out.print("inside add branch \n");
			//String sqlCommand = "INSERT INTO rentmanagement.tbl_lead(person_name,contact_number,remarks,leadtype_id,reference,email) VALUES ( '" +sdata.getVisitor_name()+ "','" + sdata.getContact_no()+  "','" +sdata.getRemarks()+ "'," +sdata.getLead_type()+ ",'"+ sdata.getReferences() +"','"+ sdata.getEmail_id() +"');";
		
			
							
			String sqlCommand = "INSERT INTO `rentmanagement`.`tbl_staff`"+ 
			"(`name`,`contact`,`email`,`address`,`city`,`state`,`zipcode`,`permanent_address`,`landline_no`,`comment`,`address_proof_list`,`id_proof_list`)"+
			"VALUES ('" +
						sdata.getName() +"','"+
						sdata.getContact_no()+"','"+
						sdata.getEmail_id()+"','"+
						sdata.getAdress()+"','"+
						sdata.getCity() +"','"+
						sdata.getState() +"','"+
						sdata.getZip() +"','"+
						sdata.getPermanent_address() +"','"+
						sdata.getLandline_no() +"','"+
						sdata.getComment() +"','"+
						sdata.getAddress_proof_list() +"','"+
						sdata.getId_proof_list()
						+"')";
			
			
			
			System.out.print(sqlCommand);
			Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
			try {
				boolean insert = ps.execute(sqlCommand);
				System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//ResultSet re =ps.executeQuery();
		//System.out.print("yyuuyyu"+re.toString());
			
		}
	 
	 public ArrayList<Staff>  GetAllStaff(Connection connection) throws Exception
		{
			ArrayList<Staff>  allfirms =new ArrayList<Staff>();
			try
			{
				//String uname = request.getParameter("uname");
				String query ="SELECT * FROM rentmanagement.tbl_staff";
				
				System.out.println(query);
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					Staff firm = new Staff();
					firm.setName(rs.getString("name"));
					firm.setContact_no(rs.getString("contact"));
					firm.setEmail_id(rs.getString("email"));
					firm.setAdress(rs.getString("address"));
					firm.setCity(rs.getString("city"));
					firm.setState(rs.getString("state"));
					firm.setZip(rs.getString("zipcode"));
					firm.setId(rs.getInt("ID"));
					firm.setPermanent_address(rs.getString("permanent_address"));
					firm.setLandline_no(rs.getString("landline_no"));			
					firm.setComment(rs.getString("comment"));
					firm.setAddress_proof_list(rs.getString("address_proof_list"));
					firm.setId_proof_list(rs.getString("id_proof_list"));
					allfirms.add(firm);
					
				}
				return allfirms;
			}
			catch(Exception e)
			{
				throw e;
			}
	}
	
	
	 public void InsertFollowup(Connection connection,LeadFollowup sdata) throws SQLException{
			System.out.print("inside add branch \n");
			//String sqlCommand = "INSERT INTO rentmanagement.tbl_lead(person_name,contact_number,remarks,leadtype_id,reference,email) VALUES ( '" +sdata.getVisitor_name()+ "','" + sdata.getContact_no()+  "','" +sdata.getRemarks()+ "'," +sdata.getLead_type()+ ",'"+ sdata.getReferences() +"','"+ sdata.getEmail_id() +"');";
		
			
							
			String sqlCommand = "insert into `rentmanagement`.`tbl_lead_followup` (`details`,`lead_id`,`next_followup_date`)values("+
			"'" +
						sdata.getDetails() +"',"+
						sdata.getLead_id()+",'"+
						sdata.getNext_followup_date()  +"')";
			
			
			
			System.out.print(sqlCommand);
			Statement ps = (Statement) connection.createStatement();//Statement(sqlCommand);
			try {
				boolean insert = ps.execute(sqlCommand);
				System.out.println("Result of insert>>>>>>>>>>>>>>>"+insert);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//ResultSet re =ps.executeQuery();
		//System.out.print("yyuuyyu"+re.toString());
			
		}
	 
	 public ArrayList<LeadFollowup>  GetFollowupDetails(Connection connection,String varLeadId) throws Exception
		{
			ArrayList<LeadFollowup>  allfirms =new ArrayList<LeadFollowup>();
			try
			{
				//String uname = request.getParameter("uname");
				String query ="SELECT * FROM rentmanagement.tbl_lead_followup where lead_id =  " + varLeadId +
						" order by date desc" ;
				
				System.out.println(query);
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					LeadFollowup firm = new LeadFollowup();
					firm.setDetails(rs.getString("details"));
					firm.setDate(rs.getString("date"));
					firm.setNext_followup_date(rs.getString("next_followup_date"));
					firm.setLead_id(rs.getInt("lead_id"));
					firm.setId(rs.getInt("ID"));
								
					allfirms.add(firm);
					
				}
				return allfirms;
			}
			catch(Exception e)
			{
				throw e;
			}
	}
	 public String  GetMenuDetails(Connection connection) throws Exception
		{
			//ArrayList<LeadFollowup>  allfirms =new ArrayList<LeadFollowup>();
			try
			{
				//String uname = request.getParameter("uname");
				/*String query ="SELECT * FROM rentmanagement.tbl_lead_followup where lead_id =  " + varLeadId +
						" order by date desc" ;
				*/
				String query="SELECT 	tm.name AS menuName, tmiii.name AS subMenuName,tmiii.html   AS subMenuHTML,tm.html AS menuHTML  FROM `tbl_menu_items` AS tmiii"+
				" INNER JOIN  `tbl_menu` AS tm ON tm.id=tmiii.menu_id	LIMIT 0, 1000";
				System.out.println(query);
				PreparedStatement ps = connection.prepareStatement(query);
				//ps.setString(1,uname);
				ResultSet rs = ps.executeQuery();
				StringBuilder sb = new StringBuilder();
				String mainMenuPrefix ="<li class=''>";
				String subMenuPrefix ="<ul>";
				String mainMenuSuffix ="</li>";
				String subMenuSuffix ="</ul>";
				Boolean mainmenu=true;
				String previous="test";
				while(rs.next())
				{
					if(mainmenu && ! rs.getString("menuName").equals(previous) ){
						if(previous!="test"){
							sb.append(subMenuSuffix);
							sb.append(mainMenuSuffix);
						}
						sb.append(mainMenuPrefix);
						sb.append(rs.getString("menuHTML"));
						sb.append(subMenuPrefix);
						
					}
					previous=rs.getString("menuName");
					sb.append(rs.getString("subMenuHTML"));
					if(previous !="test" && ! rs.getString("menuName").equals(previous) ){
						sb.append(subMenuSuffix);
						sb.append(mainMenuSuffix);
						
					}
					
					
				}
				sb.append(subMenuSuffix);
				sb.append(mainMenuSuffix);
				System.out.println(sb.toString());
				return sb.toString();
			}
			catch(Exception e)
			{
				throw e;
			}
	}
}
