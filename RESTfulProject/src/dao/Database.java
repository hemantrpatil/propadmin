package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {
	private  void readPropertyFileFromClasspath(String propertyFileName) throws IOException {
        Properties prop = new Properties();
        prop.load(Database.class.getClassLoader().getResourceAsStream(propertyFileName));
        System.out.println(propertyFileName +" loaded from Classpath::db.host = "+prop.getProperty("db.host"));
        System.out.println(propertyFileName +" loaded from Classpath::db.user = "+prop.getProperty("db.user"));
        System.out.println(propertyFileName +" loaded from Classpath::db.pwd = "+prop.getProperty("db.pwd"));
        System.out.println(propertyFileName +" loaded from Classpath::XYZ = "+prop.getProperty("XYZ"));
         
    }
	public Connection Get_Connection() throws Exception
	{
		try
		{
		String connectionURL = "jdbc:mysql://localhost:3306/rentmanagement";
		Connection connection = null;
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection = DriverManager.getConnection(connectionURL, "root", "");
	    return connection;
		}
		catch (SQLException e)
		{
		throw e;	
		}
		catch (Exception e)
		{
		throw e;	
		}
	}

}
