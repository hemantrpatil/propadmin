package dto;

public class FirmDeatilsByEvaluator {

	private String FirmName;
	private String FirmState;
	private String FirmCountry;
	private String FirmPracticeType;
	private String FirmEvaluator;
	private String FirmEvaluatorID;
	private String FirmFirmID;
	public String getFirmName() {
		return FirmName;
	}
	public String getFirmState() {
		return FirmState;
	}
	public void setFirmState(String firmState) {
		FirmState = firmState;
	}
	public String getFirmCountry() {
		return FirmCountry;
	}
	public void setFirmCountry(String firmCountry) {
		FirmCountry = firmCountry;
	}
	public String getFirmPracticeType() {
		return FirmPracticeType;
	}
	public void setFirmPracticeType(String firmPracticeType) {
		FirmPracticeType = firmPracticeType;
	}
	public String getFirmEvaluator() {
		return FirmEvaluator;
	}
	public void setFirmEvaluator(String firmEvaluator) {
		FirmEvaluator = firmEvaluator;
	}
	public void setFirmName(String firmName) {
		FirmName = firmName;
	}
	public String getFirmEvaluatorID() {
		return FirmEvaluatorID;
	}
	public void setFirmEvaluatorID(String firmEvaluatorID) {
		FirmEvaluatorID = firmEvaluatorID;
	}
	public String getFirmFirmID() {
		return FirmFirmID;
	}
	public void setFirmFirmID(String firmFirmID) {
		FirmFirmID = firmFirmID;
	}
	
}
