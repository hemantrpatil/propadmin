package dto;

public class SurveyData {
private int Objectives;
private int Expertise;
private int Responsiveness;
private int Efficiancy;
private int XYZ;
private int firmID;
private int userID;
private String Year;
public int getObjectives() {
	return Objectives;
}
public void setObjectives(int objectives) {
	Objectives = objectives;
}
public int getExpertise() {
	return Expertise;
}
public void setExpertise(int expertise) {
	Expertise = expertise;
}
public int getResponsiveness() {
	return Responsiveness;
}
public void setResponsiveness(int responsiveness) {
	Responsiveness = responsiveness;
}
public int getEfficiancy() {
	return Efficiancy;
}
public void setEfficiancy(int efficiancy) {
	Efficiancy = efficiancy;
}
public int getXYZ() {
	return XYZ;
}
public void setXYZ(int xYZ) {
	XYZ = xYZ;
}
public int getFirmID() {
	return firmID;
}
public void setfirmID(int fID) {
	firmID = fID;
}
public int getuserID() {
	return userID;
}
public void setEvaluatorID(int uID) {
	userID = uID;
}
public String getYear() {
	return Year;
}
public void setYear(String Year) {
	this.Year = Year;
}

}