package dto;

public class ChartData {
	private String firmName;
	private Float firmAverage;
	private Float totalAverage;
	public String getFirmName() {
		return firmName;
	}
	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}
	public Float getFirmAverage() {
		return firmAverage;
	}
	public void setFirmAverage(Float firmAverage) {
		this.firmAverage = firmAverage;
	}
	public Float getTotalAverage() {
		return totalAverage;
	}
	public void setTotalAverage(Float totalAverage) {
		this.totalAverage = totalAverage;
	}
	

}
