/*
SQLyog Community
MySQL - 5.6.25-log : Database - rentmanagement
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `rentmanagement`;

/*Table structure for table `tbl_branch` */

DROP TABLE IF EXISTS `tbl_branch`;

CREATE TABLE `tbl_branch` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `contact` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `address` text,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zipcode` varchar(6) DEFAULT NULL,
  `bank_name` varchar(60) DEFAULT NULL,
  `bank_account_no` varchar(35) DEFAULT NULL,
  `bank_ifsc_code` varchar(35) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_branch` */

insert  into `tbl_branch`(`ID`,`name`,`contact`,`email`,`address`,`city`,`state`,`zipcode`,`bank_name`,`bank_account_no`,`bank_ifsc_code`,`description`) values (1,'test','555555','test@mail.com','null','ciit','state','null','SBI','1111111111','sbin-2222','des');
insert  into `tbl_branch`(`ID`,`name`,`contact`,`email`,`address`,`city`,`state`,`zipcode`,`bank_name`,`bank_account_no`,`bank_ifsc_code`,`description`) values (2,'Chinchwad','2065111900','Chinchwad@teest.com','null','Pune','Maharashtra','null','ICICI','22222222222','icici22356','desc');
insert  into `tbl_branch`(`ID`,`name`,`contact`,`email`,`address`,`city`,`state`,`zipcode`,`bank_name`,`bank_account_no`,`bank_ifsc_code`,`description`) values (3,'bbn','1122222','nmnm@fffg','null','dfdffds','fdsfsd','null','AXIS','3333333','test',NULL);
insert  into `tbl_branch`(`ID`,`name`,`contact`,`email`,`address`,`city`,`state`,`zipcode`,`bank_name`,`bank_account_no`,`bank_ifsc_code`,`description`) values (4,'test','null','test@gmail1.com','null','Pune','Maharashtra','null','null','66666','56566565656','null');

/*Table structure for table `tbl_branch_staff` */

DROP TABLE IF EXISTS `tbl_branch_staff`;

CREATE TABLE `tbl_branch_staff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_branch_staff` */

/*Table structure for table `tbl_follow_up` */

DROP TABLE IF EXISTS `tbl_follow_up`;

CREATE TABLE `tbl_follow_up` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `lead_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `next_followup_date` date DEFAULT NULL,
  `Details` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_follow_up` */

/*Table structure for table `tbl_lead` */

DROP TABLE IF EXISTS `tbl_lead`;

CREATE TABLE `tbl_lead` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(50) DEFAULT NULL,
  `reference` varchar(30) DEFAULT NULL,
  `contact_number` varchar(10) DEFAULT NULL,
  `remarks` text,
  `lead_type` varchar(15) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `priority` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `req_start_date` date DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `property_type` varchar(20) DEFAULT NULL,
  `property_type_detail` varchar(50) DEFAULT NULL,
  `max_rent` int(11) DEFAULT NULL,
  `max_deposit` int(11) DEFAULT NULL,
  `min_deposit` int(11) DEFAULT NULL,
  `min_rent` int(11) DEFAULT NULL,
  `landline_no` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_lead` */

insert  into `tbl_lead`(`ID`,`person_name`,`reference`,`contact_number`,`remarks`,`lead_type`,`status`,`date`,`priority`,`email`,`req_start_date`,`location`,`property_type`,`property_type_detail`,`max_rent`,`max_deposit`,`min_deposit`,`min_rent`,`landline_no`) values (6,'test','www.makan.com','4444444444','sssssssssssss','owner',NULL,'2016-03-25 15:30:28',NULL,'test@mail.com','2016-03-17','akurdi','residential','3BHK',333,3333,1000,1000,'11111');
insert  into `tbl_lead`(`ID`,`person_name`,`reference`,`contact_number`,`remarks`,`lead_type`,`status`,`date`,`priority`,`email`,`req_start_date`,`location`,`property_type`,`property_type_detail`,`max_rent`,`max_deposit`,`min_deposit`,`min_rent`,`landline_no`) values (7,'dummy','www.magicbricks.com','2136543','1bhk good','owner',NULL,'2016-03-25 15:30:32',NULL,'duummy@test.com','2016-03-24','Chinchwad','residential','2BHK',5000,5000,1000,1000,'2222');
insert  into `tbl_lead`(`ID`,`person_name`,`reference`,`contact_number`,`remarks`,`lead_type`,`status`,`date`,`priority`,`email`,`req_start_date`,`location`,`property_type`,`property_type_detail`,`max_rent`,`max_deposit`,`min_deposit`,`min_rent`,`landline_no`) values (8,'test','www.makan.com','8928113683','null','owner',NULL,'2016-03-26 09:44:06',NULL,'test@mail.com','2016-03-25','Pimpri,Chinchwad,akurdi,new sangvi','residential','2BHK',10000,95000,1100,1000,'3333');
insert  into `tbl_lead`(`ID`,`person_name`,`reference`,`contact_number`,`remarks`,`lead_type`,`status`,`date`,`priority`,`email`,`req_start_date`,`location`,`property_type`,`property_type_detail`,`max_rent`,`max_deposit`,`min_deposit`,`min_rent`,`landline_no`) values (9,'test','www.makan.com','9288136883','test','owner',NULL,'2016-03-26 09:43:47',NULL,'test@mail.com','2016-03-25','Pimpri,Chinchwad','residential','4BHK',55000,70000,1000,1000,'4444');
insert  into `tbl_lead`(`ID`,`person_name`,`reference`,`contact_number`,`remarks`,`lead_type`,`status`,`date`,`priority`,`email`,`req_start_date`,`location`,`property_type`,`property_type_detail`,`max_rent`,`max_deposit`,`min_deposit`,`min_rent`,`landline_no`) values (10,'prana','www.housing.com','8928136883','test','owner',NULL,'2016-03-26 09:45:39',NULL,'test@mail.com','2016-03-17','Chinchwad,akurdi','residential','4BHK',15000,40000,30000,10000,'77777777');
insert  into `tbl_lead`(`ID`,`person_name`,`reference`,`contact_number`,`remarks`,`lead_type`,`status`,`date`,`priority`,`email`,`req_start_date`,`location`,`property_type`,`property_type_detail`,`max_rent`,`max_deposit`,`min_deposit`,`min_rent`,`landline_no`) values (11,'Teston31st','www.magicbricks.com','null','xhzhghh','owner',NULL,'2016-03-31 11:02:31',NULL,'test@gmail1.com','2016-03-31','Pimpri,akurdi','residential','3BHK',25000,30000,15000,5000,'2202222221');

/*Table structure for table `tbl_lead_followup` */

DROP TABLE IF EXISTS `tbl_lead_followup`;

CREATE TABLE `tbl_lead_followup` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `details` text,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `staff_id` int(10) DEFAULT NULL,
  `next_followup_date` varchar(30) DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_lead_followup` */

insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (1,'test','2016-03-20 00:42:04',NULL,'2016-03-20 00:00:00',3);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (2,'hello','2016-03-20 00:49:28',NULL,'2016-03-20 00:00:00',3);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (3,'hello again','2016-03-20 00:50:54',NULL,'2016-03-21 00:00:00',3);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (4,'my god hello again','2016-03-20 00:51:17',NULL,'2016-03-23 00:00:00',3);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (5,'test on 21st','2016-03-21 12:02:54',NULL,'2016-03-21 00:00:00',3);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (6,'dummy','2016-03-21 12:05:09',NULL,'2016-03-16 00:00:00',7);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (7,'pleasse call me  afterr two days','2016-03-23 16:36:51',NULL,'2016-03-25 00:00:00',3);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (8,'hthhyh','2016-03-25 19:23:50',NULL,'2016-03-18 00:00:00',7);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (9,'test','2016-03-26 00:51:52',NULL,'2016-03-27 00:00:00',10);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (10,'test agaiin for m testing','2016-03-26 00:52:20',NULL,'2016-03-28 00:00:00',10);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (11,'first followup','2016-03-26 09:47:30',NULL,'2016-03-26 00:00:00',8);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (12,'2nd teest','2016-03-26 10:34:08',NULL,'2016-03-26 00:00:00',8);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (15,'3rd test','2016-03-26 10:47:31',NULL,'2016-03-26T00:00:00.000Z',8);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (16,'test','2016-03-26 19:39:04',NULL,'2016-03-26T00:00:00.000Z',6);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (17,'test2','2016-03-26 19:39:40',NULL,'2016-03-26T00:00:00.000Z',0);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (18,'test2','2016-03-26 19:44:17',NULL,'2016-03-26T00:00:00.000Z',6);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (19,'test3','2016-03-26 19:44:29',NULL,'2016-03-26T00:00:00.000Z',6);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (20,'test','2016-03-29 20:47:17',NULL,'2016-03-29T00:00:00.000Z',6);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (21,'first follow up','2016-03-31 11:03:33',NULL,'2016-03-09T00:00:00.000Z',11);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (22,'2nd followup','2016-03-31 11:03:50',NULL,'2016-03-31T00:00:00.000Z',11);
insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`) values (23,'third followup','2016-03-31 11:04:23',NULL,'2016-04-07T00:00:00.000Z',11);

/*Table structure for table `tbl_lead_status` */

DROP TABLE IF EXISTS `tbl_lead_status`;

CREATE TABLE `tbl_lead_status` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_lead_status` */

/*Table structure for table `tbl_lead_type` */

DROP TABLE IF EXISTS `tbl_lead_type`;

CREATE TABLE `tbl_lead_type` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_lead_type` */

/*Table structure for table `tbl_membership_users` */

DROP TABLE IF EXISTS `tbl_membership_users`;

CREATE TABLE `tbl_membership_users` (
  `memberID` int(20) NOT NULL AUTO_INCREMENT,
  `passMD5` varchar(40) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `signupDate` date DEFAULT NULL,
  `groupID` int(10) unsigned DEFAULT NULL,
  `isBanned` tinyint(4) DEFAULT NULL,
  `isApproved` tinyint(4) DEFAULT NULL,
  `custom1` text,
  `custom2` text,
  `custom3` text,
  `custom4` text,
  `comments` text,
  `pass_reset_key` varchar(100) DEFAULT NULL,
  `pass_reset_expiry` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`memberID`),
  KEY `groupID` (`groupID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_membership_users` */

insert  into `tbl_membership_users`(`memberID`,`passMD5`,`email`,`signupDate`,`groupID`,`isBanned`,`isApproved`,`custom1`,`custom2`,`custom3`,`custom4`,`comments`,`pass_reset_key`,`pass_reset_expiry`) values (1,'UGFzcw==','test@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tbl_menu` */

DROP TABLE IF EXISTS `tbl_menu`;

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `html` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_menu` */

insert  into `tbl_menu`(`id`,`name`,`html`) values (1,'Dashboard','<a href=\'#/staff\'><i class=\"fa fa-flask\">');
insert  into `tbl_menu`(`id`,`name`,`html`) values (2,'Staff','<a href=\'#/staff\'><i class=\"fa fa-flask\"></i>Staff</a>');
insert  into `tbl_menu`(`id`,`name`,`html`) values (3,'Branch','<a href=\'#/branch\'><i class=\"fa fa-bar-chart-o\"></i>Branch</a>');
insert  into `tbl_menu`(`id`,`name`,`html`) values (4,'Accounts',' <a href=\'#\'><i class=\"fa fa-flask\"></i>Accounts</a>');
insert  into `tbl_menu`(`id`,`name`,`html`) values (5,'HelpDesk','<a href=\'#/servicereq\'><i class=\"fa fa-table\"></i>Help Desk</a>');

/*Table structure for table `tbl_menu_items` */

DROP TABLE IF EXISTS `tbl_menu_items`;

CREATE TABLE `tbl_menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `html` varchar(200) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_menu_items` */

insert  into `tbl_menu_items`(`id`,`name`,`html`,`menu_id`) values (1,'AddLead','<li><a href=\'#/addlead\'>Add Lead</a></li>',5);
insert  into `tbl_menu_items`(`id`,`name`,`html`,`menu_id`) values (2,'Lead','<li><a href=\'#/showallleads\'>Lead</a></li>',5);
insert  into `tbl_menu_items`(`id`,`name`,`html`,`menu_id`) values (3,'AddNewBranch',' <li><a href=\'#/addbranch\' >Add New Branch</a></li> ',3);
insert  into `tbl_menu_items`(`id`,`name`,`html`,`menu_id`) values (4,'BranchList',' <li><a href=\'#/branch\'>Branch List</a></li>',3);
insert  into `tbl_menu_items`(`id`,`name`,`html`,`menu_id`) values (5,'StaffList',' <li><a href=\'#/staff\' >Staff List</a></li>',2);
insert  into `tbl_menu_items`(`id`,`name`,`html`,`menu_id`) values (6,'AddNewStaffMember','<li><a href=\'#/addstaff\' >Add New Staff Member</a></li>',2);

/*Table structure for table `tbl_person_menu_items` */

DROP TABLE IF EXISTS `tbl_person_menu_items`;

CREATE TABLE `tbl_person_menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `menu_items_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_person_menu_items` */

/*Table structure for table `tbl_role` */

DROP TABLE IF EXISTS `tbl_role`;

CREATE TABLE `tbl_role` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_role` */

/*Table structure for table `tbl_role_staff` */

DROP TABLE IF EXISTS `tbl_role_staff`;

CREATE TABLE `tbl_role_staff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_role_staff` */

/*Table structure for table `tbl_staff` */

DROP TABLE IF EXISTS `tbl_staff`;

CREATE TABLE `tbl_staff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `contact` varchar(10) DEFAULT NULL,
  `address` text,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zipcode` varchar(6) DEFAULT NULL,
  `image_path` text,
  `permanent_address` varchar(100) DEFAULT NULL,
  `landline_no` varchar(15) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `address_proof_list` varchar(200) DEFAULT NULL,
  `id_proof_list` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_staff` */

insert  into `tbl_staff`(`ID`,`name`,`email`,`contact`,`address`,`city`,`state`,`zipcode`,`image_path`,`permanent_address`,`landline_no`,`comment`,`address_proof_list`,`id_proof_list`) values (1,'testEmployee1','test@mail.com','22222444','Chinchwad Pune','pune','maharashtra','411033',NULL,'jalgaon','02026777691','Address proof not provvided',NULL,NULL);
insert  into `tbl_staff`(`ID`,`name`,`email`,`contact`,`address`,`city`,`state`,`zipcode`,`image_path`,`permanent_address`,`landline_no`,`comment`,`address_proof_list`,`id_proof_list`) values (3,'testEmp1','test1@mail.com','111111','Akurdi','Pune','Maharashtrra','411035',NULL,'Amravati','123456788','ID proof not  provvided',NULL,NULL);
insert  into `tbl_staff`(`ID`,`name`,`email`,`contact`,`address`,`city`,`state`,`zipcode`,`image_path`,`permanent_address`,`landline_no`,`comment`,`address_proof_list`,`id_proof_list`) values (4,'testEmp2','test2@mail.com','111111','chinchwad station','Pune','Maharashtra','411033',NULL,'Pune','123345678','All documents ok','Adharcard,Passport','Passport');

/* Procedure structure for procedure `sp_add_lead` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_add_lead` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_lead`(
   IN  person_name varchar(50),
   IN  reference varchar(30), 
   IN  contact_number varchar(10), 
   IN  remarks text, 
   IN  lead_type varchar(15), 
   IN  `status` varchar(20), 
   
   IN  priority varchar(10), 
   IN  email varchar(30), 
   IN   req_start_date date, 
   IN   location varchar(200), 
   IN	property_type varchar(20), 
   IN	property_type_detail varchar(50), 
   IN	max_rent int, 
   IN	max_deposit int,
   IN	min_rent INT, 
   IN	min_deposit INT,
   IN	landline_no INT)
BEGIN
INSERT INTO `rentmanagement`.`tbl_lead` 
	(
	`person_name`, 
	`reference`, 
	`contact_number`, 
	`remarks`, 
	`lead_type`, 
	
	`priority`, 
	`email`, 
	`req_start_date`, 
	`location`, 
	`property_type`, 
	`property_type_detail`, 
	`max_rent`, 
	`max_deposit`,
	`min_rent`, 
	`min_deposit`,
	`landline_no`
	
	)
 VALUES(
  person_name,
  reference, 
  contact_number, 
  remarks, 
  lead_type, 
  
  priority, 
  email, 
  req_start_date, 
  location, 
  property_type, 
  property_type_detail, 
  max_rent, 
  max_deposit,
  min_rent, 
  min_deposit,
  landline_no);
  
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
