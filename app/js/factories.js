'use strict';
 
app.factory('LeadService', ['$http', '$q','$filter', function($http, $q,$filter){
    var GetAllLeads={};
     var selectedData={};

    return {
         getSelectedData: function(){ return selectedData},
            fetchAllLeads: function() {
                    var req={
                                method: 'GET',
                                url: 'http://localhost:8080/RESTfulProject/REST/WebService/GetAllLeads',
                                withCredentials: true,
                                headers: {
                                      'Content-Type': 'application/json'
                                      
                                  }
                               };
                         return $http(req)
                            .then(
                                    function(response){
                                        GetAllLeads=response.data;
                                        sessionStorage.GetAllLeads =angular.toJson(GetAllLeads);
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log(angular.toJson(errResponse));
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

            fetchAllFollowups: function(leadId) {
                    return $http.get('http://localhost:8080/RESTfulProject/REST/WebService/GetFollowupDetails/' + leadId)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
             fetchLeadSpecificData:function(leadId){
                //alert(angular.toJson(GetAllLeads));
                if(GetAllLeads=[]) GetAllLeads = angular.fromJson(sessionStorage.GetAllLeads);
                var single_object = $filter('filter')(GetAllLeads, function (d) {return d.id == leadId;})[0];
                //alert(angular.toJson(single_object));
                selectedData=single_object;
                
                //alert(angular.toJson(GetAllLeads));
                return single_object;
             },
             
             addFollowup: function(user){
                    return $http.post('http://localhost:8080/RESTfulProject/REST/WebService/InsertFollowup/', user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            createUser: function(user){
                    return $http.post('http://localhost:8080/Spring4MVCAngularJSExample/user/', user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateUser: function(user, id){
                    return $http.put('http://localhost:8080/Spring4MVCAngularJSExample/user/'+id, user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deleteUser: function(id){
                    return $http.delete('http://localhost:8080/Spring4MVCAngularJSExample/user/'+id)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting user');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);
app.factory('BranchService', ['$http', '$q', '$filter',function($http, $q,$filter){
    var GetAllBranches={};
    var GetAllStaff=[];
 
    return {
         
            fetchAllBranches: function() {
                    return $http.get('http://localhost:8080/RESTfulProject/REST/WebService/GetAllBranches')
                            .then(
                                    function(response){
                                       
                                        GetAllBranches=response.data;
                                        sessionStorage.GetAllBranches =angular.toJson(GetAllBranches);
                                       //alert(angular.toJson( GetAllBranches));
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             fetchBranchSpecificData:function(BranchId){
                //alert(angular.toJson(GetAllLeads));
                if(GetAllBranches=[]) GetAllBranches = angular.fromJson(sessionStorage.GetAllBranches);
                var single_object = $filter('filter')(GetAllBranches, function (d) {return d.id == BranchId;})[0];
                
                
                //alert(angular.toJson(GetAllLeads));
                return single_object;
             },
             
             fetchAllStaff: function(BranchId) {
                    return $http.get('http://localhost:8080/RESTfulProject/REST/WebService/GetAllStaff/' )//+ BranchId)
                            .then(
                                    function(response){
                                       GetAllStaff= response.data;
                                       sessionStorage.GetAllStaff =angular.toJson(GetAllStaff);
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching staff data');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            fetchStaffMemberData:function(MemberId){
                //alert(angular.toJson(GetAllLeads));
                if(GetAllStaff=[]) GetAllStaff = angular.fromJson(sessionStorage.GetAllStaff);
                var single_object = $filter('filter')(GetAllStaff, function (d) {return d.id == MemberId;})[0];
                
                
                //alert(angular.toJson(GetAllLeads));
                return single_object;
             },
            createUser: function(user){
                    return $http.post('http://localhost:8080/Spring4MVCAngularJSExample/user/', user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateUser: function(user, id){
                    return $http.put('http://localhost:8080/Spring4MVCAngularJSExample/user/'+id, user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deleteUser: function(id){
                    return $http.delete('http://localhost:8080/Spring4MVCAngularJSExample/user/'+id)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting user');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);
app.factory('StaffService', ['$http', '$q', function($http, $q){
 
    return {
         
            fetchAllStaff: function() {
                    return $http.get('http://localhost:8080/RESTfulProject/REST/WebService/GetAllStaff')
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            createUser: function(user){
                    return $http.post('http://localhost:8080/Spring4MVCAngularJSExample/user/', user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateUser: function(user, id){
                    return $http.put('http://localhost:8080/Spring4MVCAngularJSExample/user/'+id, user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deleteUser: function(id){
                    return $http.delete('http://localhost:8080/Spring4MVCAngularJSExample/user/'+id)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting user');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);

app.factory('AuthenticationService', AuthenticationService);
 
    AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'UserService'];
    function AuthenticationService($http, $cookieStore, $rootScope, $timeout, UserService) {
        var service = {};
 
        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
 
        return service;
 
        function Login(username, password, callback) {
 
            /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/
      

            $timeout(function () {
                var response;
                 var encodedPassword=Base64.encode( password);
                
                UserService.GetByUsername(username,encodedPassword)
                    .then(function (user) {

                       // if (user !== null && user.password === password) {
                         if (user !== null && user.password === encodedPassword) {
                            response = { success: true };
                        } else {
                            response = { success: false, message: 'Username or password is incorrect' };
                        }
                        callback(response);
                    });
            }, 1000);
 
            /* Use this for real authentication
             ----------------------------------------------*/
            //$http.post('/api/authenticate', { username: username, password: password })
            //    .success(function (response) {
            //        callback(response);
            //    });
 
        }
 
        function SetCredentials(username, password) {
            var authdata = Base64.encode(username + ':' + password);
 
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };
 
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        }
 
        function ClearCredentials() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic';
        }
    }
   var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
 /*   // Base64 encoding service used by AuthenticationService
    var Base64 = {
 
        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
 
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
 
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
 
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
 
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
 
                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
 
            return output;
        },
 
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
 
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
            do {
                enc1 = this.keyStr.indexOf(input.charAt(i++));
                enc2 = this.keyStr.indexOf(input.charAt(i++));
                enc3 = this.keyStr.indexOf(input.charAt(i++));
                enc4 = this.keyStr.indexOf(input.charAt(i++));
 
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
 
                output = output + String.fromCharCode(chr1);
 
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
 
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
 
            } while (i < input.length);
 
            return output;
        }
    };
 */
app.factory('UserService', UserService);
 
    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};
 
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
 
        return service;
 
        function GetAll() {
            return $http.get('/api/users').then(handleSuccess, handleError('Error getting all users'));
        }
 
        function GetById(id) {
            return $http.get('/api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }
 
        function GetByUsername(username,password) {

            return  $http.get('http://localhost:8080/RESTfulProject/REST/WebService/GetLoginDetails/' + username +'/'+ password).then(handleSuccess, handleError('Error getting user by username'));
        }
 
        function Create(user) {
            return $http.post('/api/users', user).then(handleSuccess, handleError('Error creating user'));
        }
 
        function Update(user) {
            return $http.put('/api/users/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }
 
        function Delete(id) {
            return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
        }
 
        // private functions
 
        function handleSuccess(res) {
            return res.data;
        }
 
        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

    app.factory('propertyService', ['$http', '$q', '$filter',function($http, $q,$filter){
    var GetAllProprety={};
    var GetAllStaff=[];
 
    return {
         
            fetchAllProperty: function() {
                    return $http.get('http://localhost:8080/RESTfulProject/REST/WebService/GetAllProprety')
                            .then(
                                    function(response){
                                       
                                        GetAllProprety=response.data;
                                        sessionStorage.GetAllProprety =angular.toJson(GetAllProprety);
                                       //alert(angular.toJson( GetAllProprety));
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             fetchBranchSpecificData:function(BranchId){
                //alert(angular.toJson(GetAllLeads));
                if(GetAllProprety=[]) GetAllProprety = angular.fromJson(sessionStorage.GetAllProprety);
                var single_object = $filter('filter')(GetAllProprety, function (d) {return d.id == BranchId;})[0];
                
                
                //alert(angular.toJson(GetAllLeads));
                return single_object;
             }
         }}
        ]);
