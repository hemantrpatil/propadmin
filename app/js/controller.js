var app= angular.module('Myapp', ['ngRoute','ngCookies','ngFileSaver','ui.bootstrap','ui.grid','ui.grid.selection', 'ui.grid.exporter','ui.mask','ngSanitize', 'ngCsv']);
app.config(function($httpProvider) {
    //Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;
});
app.config(function($routeProvider) {
	$routeProvider
	.when('/',{
		templateUrl: 'app/home.html'
	})
	
	.when('/home',{
		templateUrl: 'app/home.html'
	})

  .when('/feature',{
    templateUrl: 'app/admin/feature.html'
  })
	
	.when('/property',{ 
		templateUrl: 'app/property/available_property.html',
     controller:'showpropertyController'
	})
	.when('/property_details',{ 
		templateUrl: 'app/property_details.html'
	})
	
	.when('/addproperty',{ 
		templateUrl: 'app/property/add_property.html',
    controller:'addpropertycontroller'
		//templateUrl: 'app/add_property.html'
	})
	.when('/addcommercial',{ 
		templateUrl: 'app/add_commercial.html'
	})
	.when('/addpg',{ 
		templateUrl: 'app/add_pg.html'
	})
	
	.when('/staff',{ 
		templateUrl: 'app/staff/staff.html',
		//templateUrl: 'app/stafflist.html',
		controller:'showStaffcontroller'
	})
	.when('/addstaff',{
		templateUrl: 'app/staff/addstaff.html',
		//templateUrl: 'app/add_staff.html',
		controller: 'staffController'
	})
	.when('/staffdetail/:memberId',{
		templateUrl: 'app/staff/staffdetail.html',
		//templateUrl: 'app/add_staff.html',
		controller: 'staffMemberDetailcontroller',
		controllerAs :'member'
	})
	.when('/rmprofile',{
		templateUrl: 'app/rm_profile.html'
	})
	.when('/branch',{
		templateUrl: 'app/branch/branch.html',
		controller:'showBranchController'
	})
	.when('/addbranch',{
		templateUrl: 'app/branch/addbranch.html',
		controller: 'branchController'
	})
	.when('/servicereq',{
		templateUrl: 'app/service_request.html'
	})
	.when('/servicerequested',{
		templateUrl: 'app/requested_services.html'
	})
	.when('/showallleads',{
		templateUrl: 'app/lead/customer_leadGrid.html',
		controller: 'showleadController',
		controllerAs:'leadctrl'
	})
	.when('/viewlead/:leadId',{
		templateUrl: 'app/lead/view_lead.html',
		controller:'leadFollowupcontroller as followup'
	})
	.when('/viewrequestedservice',{
		templateUrl: 'app/view_requested_service.html'
	})
	.when('/closedrequestedservice',{
		templateUrl: 'app/closed_requested_service.html'
	})
	.when('/branchprofile/:branchId',{
		templateUrl: 'app/branch/branch_profile.html',
		controller : 'branchDetailcontroller',
		controllerAs : 'branch'
		
	})
	.when('/collectorofficer',{
		templateUrl: 'app/collector_off.html'
	})
	/*.when('/addcollector',{
		templateUrl: 'add_collection_officer.html'
	})*/
	.when('/collectorprofile',{
		templateUrl: 'app/collector_profile.html'
	})
	.when('/ownerlist',{ 
		templateUrl: 'owner_list.html'
	})
	.when('/ownerregistration',{ 
		templateUrl: 'owner_registration.html'
	})
	.when('/ownerprofile',{ 
		templateUrl: 'app/owner_profile.html'
	})
	.when('/tenentlist',{ 
		templateUrl: 'app/tenent_list.html'
	})
	.when('/tenentprofile',{ 
		templateUrl: 'app/tenent_profile.html'
	})
	.when('/tenentregistration',{ 
		templateUrl: 'app/tenent_registration.html'
	})
	.when('/addlead',{ 
		templateUrl: 'app/lead/addlead.html',
		controller:'leadController'
	})
	.when('/login',{ 

		//templateUrl: 'app/authentication/login.html'

		templateUrl: 'app/login.html',
		controller: 'LoginController'
	})
	           
	
	
	
	
});
 app.run( ['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
    	//alert("test login1");
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
 	
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        	//alert("test login2");
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }
 
 ]);


app.controller('addpropertycontroller',['$scope','$http','$location',function($scope,$http,$location){

$scope.propertyform={};

$scope.addproperty = function(){

  var tempdate=$scope.propertyform.keyrdate;
  $scope.propertyform.keyrdate = tempdate.getFullYear() + "-" + ( tempdate.getMonth() >9 ? (tempdate.getMonth()+1) : "0" + (tempdate.getMonth() +1) )+ "-"  + (tempdate.getDate() >=10 ? tempdate.getDate() : "0" + tempdate.getDate());

  $scope.propertyform.society_amenities =$scope.propertyform.society_amenities.join();

  alert(angular.toJson($scope.propertyform));
      var req={
  method: 'POST',
  url: 'http://localhost:8080/RESTfulProject/REST/WebService/InsertProperty',
  headers: {
        'Content-Type': 'application/json'}, data:angular.toJson($scope.propertyform)
   };
        
    $http(req).then(
                               function(d) {
                                    $scope.propertyform={};
                                    alert(angular.toJson(self.propertyform));
                               },
                                function(errResponse){
                                    console.error('Error while insertng property');
                                }
                       );
        $location.path('/property');      
}
}]);


app.controller('showpropertyController',['$scope','propertyService',function($scope,propertyService){
  

          var self = this;
        $scope.properties='';
          /*self.property=[];*/
               
          self.fetchAllProperty = function(){
              propertyService.fetchAllProperty()
                  .then(
                               function(d) {
                                 $scope.properties=d;
                                    self.property = d;
                                    /* alert(angular.toJson( $scope.properties));*/
                                     $scope.getArray= $scope.properties;
                               },
                                function(errResponse){
                                  console.log(errResponse);
                                    console.error('Error while fetching property');
                                }
                       );
          };
          self.fetchAllProperty();
}]);



app.controller('leadController',['$scope','$http','$location',function($scope,$http,$location){
	
$scope.leadform={};

$scope.Addlead = function(){
	var tempdate=$scope.leadform.req_start_date;
  $scope.leadform.req_start_date = tempdate.getFullYear() + "-" + ( tempdate.getMonth() >9 ? (tempdate.getMonth()+1) : "0" + (tempdate.getMonth() +1) )+ "-"  + (tempdate.getDate() >=10 ? tempdate.getDate() : "0" + tempdate.getDate());

	// alert(angular.toJson($scope.leadform.location.join()));
	$scope.leadform.location =$scope.leadform.location.join();
    	var req={
 	method: 'POST',
 	url: 'http://localhost:8080/RESTfulProject/REST/WebService/InsertLead',
 	headers: {
  		  'Content-Type': 'application/json'}, data:angular.toJson($scope.leadform)
   };
 				
 		$http(req);
    		$location.path('/showallleads');
    	
}

}]);

app.controller('showleadController',['$scope','$location','LeadService','FileSaver','uiGridConstants',function($scope,$location,LeadService,FileSaver,uiGridConstants){
	//$scope.$scope = $scope;

          var self = this;
          self.lead={visitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.leads=[];
          $scope.someProp = 'abc',
            $scope.showDetails = function(id){
                   //alert(angular.toJson(id.entity.id));
                   $location.path('/viewlead/'+ id.entity.id);
                };
           $scope.gridOptions = {
						      	enableFiltering: true,
						    		onRegisterApi: function(gridApi){
						      		$scope.gridApi = gridApi;},
           						enableGridMenu: true,
							    enableSelectAll: true,
							    exporterCsvFilename: 'myFile.csv',
							    exporterPdfDefaultStyle: {fontSize: 9},
							    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
							    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
							    exporterPdfHeader: { text: "Ajinka Real State ", style: 'headerStyle' },
							    exporterPdfFooter: function ( currentPage, pageCount ) {
							      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
							    },
							    exporterPdfCustomFormatter: function ( docDefinition ) {
							      docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
							      docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
							      return docDefinition;
							    },
							    exporterPdfOrientation: 'portrait',
							    exporterPdfPageSize: 'LETTER',
							    exporterPdfMaxGridWidth: 500,
							    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
							   


           					}
          
                              
            $scope.gridOptions.columnDefs = [
         { name: 'visitor_name' },
         { name: 'contact_no'},
         { name: 'lead_type'},
         { name: 'max_rent'},
         { name: 'max_deposit'},
         { name: 'location'},
         { name: 'property_type'},
         { name: 'property_type_details'},
         
         { name: 'details',enableFiltering: false,
             cellTemplate:'<div class="ui-grid-cell-contents" ><button class="btn btn-primary btn-xs btn-block" ng-click="grid.appScope.showDetails(row)">Details</button></div>' 
         }

         /*,
         { name: 'delete',cellTemplate:'<div class="ui-grid-cell-contents ng-binding ng-scope"><button class="btn btn-danger {{getExternalScopes().deleteButtonClass(row)}} btn-xs btn-block" ng-click="getExternalScopes().delete($event, row)"><span class="glyphicon glyphicon-trash"></span></button></div>'}
*/
       ];     
          self.fetchAllLeads = function(){
              LeadService.fetchAllLeads()
                  .then(
                               function(d) {
                                    self.leads = d;
                                     $scope.gridOptions.data=d;
                                    
                               },
                                function(errResponse){
                                	console.log(errResponse);
                                    console.error('Error while fetching leads');
                                }
                       );
          };
          self.fetchAllLeads();

           $scope.toggleFiltering = function(){
    $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
    $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
  };

 
 
 
  $scope.download = function() {
  	
  	 var t = JSON.stringify( $scope.leads);
    var data = new Blob(t, { type: 'text/html;charset=utf-8' });
    FileSaver.saveAs(data, 'text.txt');
  };
}]);

app.controller('branchController',['$scope','$http','$location',function($scope,$http,$location){

  $http.get('js/state.json').success(function(response){
      $scope.myData = response;
    });
	
$scope.branchForm={};

$scope.addBranch = function(){
	
	alert(angular.toJson($scope.branchForm));
    	var req={
 	method: 'POST',
 	url: 'http://localhost:8080/RESTfulProject/REST/WebService/InsertBranch',
 	headers: {
  		  'Content-Type': 'application/json'}, data:angular.toJson($scope.branchForm)
   };
 				
 		$http(req).then(
                               function(d) {
                                    $scope.branchForm={};
                               },
                                function(errResponse){
                                    console.error('Error while insertng branch');
                                }
                       );
    		$location.path('/branch');    	
}
}]);
app.controller('showBranchController',['$scope','BranchService',function($scope,BranchService){
	

          var self = this;
         // self.branch={visitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.branches=[];
               
          self.fetchAllBranches = function(){
              BranchService.fetchAllBranches()
                  .then(
                               function(d) {
                                    self.branches = d;
                                     $scope.getArray=self.branches;
                               },
                                function(errResponse){
                                	console.log(errResponse);
                                    console.error('Error while fetching branches');
                                }
                       );
          };
          self.fetchAllBranches();
}]);

app.controller('staffController',['$scope','$http','$location',function($scope,$http,$location){
	
$scope.staffForm={};

$scope.addStaff = function(){ 
	
	/*alert(angular.toJson($scope.staffForm));*/
    	var req={
 	method: 'POST',
 	url: 'http://localhost:8080/RESTfulProject/REST/WebService/InsertStaffMember',
 	headers: {
  		  'Content-Type': 'application/json'}, data:angular.toJson($scope.staffForm)
   };
 				
 		$http(req).then(
                               function(d) {
                                    $scope.staffForm={};
                               },
                                function(errResponse){
                                    console.error('Error while insertng staff');
                                }
                       );
    		$location.path('/feature  ');
    	
}
}]);

app.controller('showStaffcontroller',['$scope','StaffService',function($scope,StaffService){
	

          var self = this;
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.allStaff=[];

               
          self.fetchAllStaff = function(){
              StaffService.fetchAllStaff()
                  .then(
                               function(d) {
                                    self.allStaff = d;
                                   /* alert(angular.toJson(self.allStaff));*/
                                    $scope.getArray=self.allStaff;
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };
          self.fetchAllStaff();
          $scope.getHeader = function () {return ["Staff Name", "Email Id","Mobile No","Local Address","City","State","Zip Code","id","Residential Address","Landline No","Comment"]};
}]);

app.controller('leadFollowupcontroller',['$scope','$routeParams','LeadService',function($scope,$routeParams,LeadService){
	
		 var leadId =  $routeParams.leadId;
		$scope.isCollapsed=true;
          var self = this;
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.allFollowups=[];
          self.newFollowup={lead_id :leadId };
               
          self.fetchAllFollowups = function(leadId){
              LeadService.fetchAllFollowups(leadId)
                  .then(
                               function(d) {
                                    self.allFollowups = d;
                                    $scope.getArray=self.allFollowups;
                                    alert(angular.toJson(self.allFollowups));
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };

          self.addFollowup = function(){ 
          		var tempdate=self.newFollowup.next_followup_date;
		self.newFollowup.next_followup_date =new Date(tempdate.getFullYear() + "-" + ( tempdate.getMonth() >9 ? (tempdate.getMonth()+1) : "0" + (tempdate.getMonth() +1) )+ "-"  + (tempdate.getDate() >=10 ? tempdate.getDate() : "0" + tempdate.getDate()));

              LeadService.addFollowup(self.newFollowup)
                  .then(
                               function(d) {
                                     d;
                                     self.newFollowup={};
                                     self.fetchAllFollowups(leadId);
                                     self.newFollowup={lead_id :leadId };
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };
           self.leadDetails=LeadService.fetchLeadSpecificData(leadId);
           
          self.fetchAllFollowups(leadId);
          $scope.getHeader = function () {return ["id", "LeadID","date","Next Follow up date","Remarks"]};
          
          	
}]);

app.controller('branchDetailcontroller',['$scope','$routeParams','BranchService',function($scope,$routeParams,BranchService){
	
		 var branchId =  $routeParams.branchId;
		
          var self = this;
          
          
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.allStaff=[];
         
               
          self.fetchAllStaff = function(branchId){
              BranchService.fetchAllStaff(branchId)
               .then(
                               function(d) {
                                    self.allStaff = d;
                                    
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };

        
           self.branchDetails=BranchService.fetchBranchSpecificData(branchId);
           
          self.fetchAllStaff(branchId);
         
          	
}]);

app.controller('staffMemberDetailcontroller',['$scope','$routeParams','BranchService',function($scope,$routeParams,BranchService){
	
		 var memberId =  $routeParams.memberId;
		
          var self = this;
          /*alert(memberId);*/
          
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.memberDetails=BranchService.fetchStaffMemberData(memberId);
          	
}]);

app.controller('LoginController', LoginController);
 
    LoginController.$inject = ['$location', 'AuthenticationService','$scope' ];
    function LoginController($location, AuthenticationService,$scope) {
        var vm = this;
        $scope.username ='';
        $scope.password='';
        $scope.result='';
     
        //vm.login = login;
 
        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();
 
        $scope.login=function() {
            vm.dataLoading = true;
            $scope.result='';
            AuthenticationService.Login( $scope.username, $scope.password, function (response) {
                if (response.success) {
                	//alert(response);
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path('/');
                } else {
                	//alert("fail "+response);
                    //$scope.result="Invalid user id or Password";
                    angular.element('#result').addClass('alert alert-danger');
                     angular.element('#result').html("Invalid user id or Password");
                    vm.dataLoading = false;
                }
            });
        };
    }

 